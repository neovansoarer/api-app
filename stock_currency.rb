require 'stock_quote'
require 'eu_central_bank'

@bank = EuCentralBank.new
@bank.update_rates

def usd_to_krw usd
  result = usd * @bank.exchange(100, "USD", "KRW")
  result.to_s.reverse.gsub(/(\d{3})(?=\d)/, '\\1,').reverse
end

DATA.each_line do |c|
  c.chomp!
  stock = StockQuote::Stock.quote c
  puts "#{stock.name}(#{c}) 의 가격은 #{stock.last_trade_price_only}$ / #{usd_to_krw stock.last_trade_price_only}￦"
end

__END__
AMZN
GOOGL
FB
TSLA
AAPL