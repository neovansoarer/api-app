require 'pry'
require 'forecast_io'
require 'geocoder'

print '장소를 입력하세요 : '
input = gets.chomp
cord = Geocoder.coordinates(input)

ForecastIO.configure do |c|
  c.api_key = 'c3c46f2ceb471e5d5bf6cd29b5708bfe'
end

forecast = ForecastIO.forecast(cord[0], cord[1])
f = forecast.currently

puts f.summary
puts ((f.apparentTemperature - 32) * 5 / 9).round(1)
